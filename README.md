- 👋 Hi, I’m @Simon-Lozada
- 👀 I am interested in cybersecurity, python and programming in general
- 🌱 I am currently learning cybersecurity and programming with python
- 💞️ I am looking to collaborate on tools and process automation
- 📫 You can contact me through my email: "simon.e.lozada@gmail.com"

<!---
Simon-Lozada/Simon-Lozada is a ✨ special ✨ repository because its `README.md` (this file) appears on your GitHub profile.
You can click the Preview link to take a look at your changes.
--->
